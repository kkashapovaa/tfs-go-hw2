package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

type Subscriber struct {
	Email   string `json:"email"`
	Created string `json:"created_at"`
}

type User struct {
	Name    string       `json:"Nick"`
	Email   string       `json:"Email"`
	Created string       `json:"Created_at"`
	Subs    []Subscriber `json:"Subscribers"`
}

type Result struct {
	ID    int64        `json:"id"`
	From  string       `json:"from"`
	To    string       `json:"to"`
	Paths []Subscriber `json:"path,omitempty"`
}

func main() {
	users, err := readJSONFile("users.json")
	if err != nil {
		log.Fatal(err)
	}

	dataInput, err := readCSVFile("input.csv")
	if err != nil {
		log.Fatal(err)
	}

	result := getMinWayForUsers(dataInput, users)

	err = writeJSONFile("result.json", result)
	if err != nil {
		log.Fatal(err)
	}
}

func readJSONFile(filename string) ([]User, error) {
	file, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("can't read file: %s", err)
	}

	var obj []User

	err = json.Unmarshal(file, &obj)
	if err != nil {
		return nil, fmt.Errorf("no unmarshal: %s", err)
	}

	return obj, nil
}

func readCSVFile(filename string) ([][]string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, fmt.Errorf("can`t open file: %s", err)
	}
	defer file.Close()
	reader := csv.NewReader(file)

	dataInput, err := reader.ReadAll()
	if err != nil {
		return nil, fmt.Errorf("can`t read file: %s", err)
	}

	return dataInput, nil
}

func writeJSONFile(filename string, result []Result) error {
	file, err := os.Create(filename)
	if err != nil {
		return fmt.Errorf("can't create file %s", err)
	}
	defer file.Close()

	writer := json.NewEncoder(file)
	writer.SetIndent("   ", "\t")

	err = writer.Encode(result)
	if err != nil {
		return fmt.Errorf("can`t encode: %s", err)
	}

	return nil
}

func getMinWayForUsers(dataInput [][]string, users []User) []Result {
	dataUser := convertSliceToMap(users)
	result := make([]Result, 0, len(dataInput))

	for count, value := range dataInput {
		from := value[0]
		to := value[1]
		tmp := dataUser[to]
		sub := Subscriber{to, tmp.Created}
		userWay := bfs(dataUser, sub, from)
		count++
		r := Result{int64(count), from, to, userWay}
		result = append(result, r)
	}

	return result
}

func convertSliceToMap(data []User) map[string]User {
	userMap := make(map[string]User)
	for _, user := range data {
		userMap[user.Email] = user
	}

	return userMap
}

func createVisited(dataUser map[string]User) map[string]bool {
	visited := make(map[string]bool, len(dataUser))

	for key := range dataUser {
		visited[key] = false
	}

	return visited
}

func bfs(dataUser map[string]User, startEmail Subscriber, endingEmail string) []Subscriber {
	if startEmail.Email == endingEmail {
		return nil
	}

	visited := createVisited(dataUser)

	var queue [][]Subscriber

	var line []Subscriber

	line = append(line, startEmail)
	queue = append(queue, line)
	visited[startEmail.Email] = true

	for len(queue) > 0 {
		current := queue[0]
		queue = queue[1:]
		length := len(current)
		nextItem := current[length-1]
		tmpUser, ok := dataUser[nextItem.Email]

		if ok {
			for _, next := range tmpUser.Subs {
				if !visited[next.Email] {
					if next.Email == endingEmail {
						reverse(current[1:])
						return current[1:]
					}

					visited[next.Email] = true
					line := make([]Subscriber, len(current))
					copy(line, current)

					line = append(line, next)
					queue = append(queue, line)
				}
			}
		}
	}

	return nil
}

func reverse(s []Subscriber) {
	length := len(s)

	for i := 0; i < len(s)/2; i++ {
		s[i], s[length-1-i] = s[length-1-i], s[i]
	}
}
